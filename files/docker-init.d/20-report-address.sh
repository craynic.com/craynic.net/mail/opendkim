#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$OPENDKIM_REPORT_ADDRESS" ]]; then
  exit 0
fi

CFG_FILE="/etc/opendkim/opendkim.conf"

sed -i "/^#\?ReportAddress*/c\\ReportAddress $OPENDKIM_REPORT_ADDRESS" -- "$CFG_FILE"
sed -i "/^#\?SendReports*/c\\SendReports Yes" -- "$CFG_FILE"
