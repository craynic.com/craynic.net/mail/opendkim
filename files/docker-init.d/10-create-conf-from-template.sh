#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$OPENDKIM_SQL_DSN" ]]; then
  echo "SQL credentials not set, cannot start DKIM signing..." >/dev/stderr
  exit 1
fi

CFG_FILE="/etc/opendkim/opendkim.conf"
TEMPLATE_FILE="$CFG_FILE.template"

# shellcheck disable=SC2016
envsubst '${OPENDKIM_SQL_DSN}' < "$TEMPLATE_FILE" > "$CFG_FILE"
