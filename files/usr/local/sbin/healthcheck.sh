#!/usr/bin/env bash

set -Eeuo pipefail

PORTS=( "$@" )
CMD_SS="ss"
CMD_SUPERVISORCTL="supervisorctl"

# check open ports
if (( ${#PORTS[@]} != 0 )); then
	[[ -x $(which "$CMD_SS") ]] || { echo "$CMD_SS not found" >&2; exit 2; }

	SS_OUT=$("$CMD_SS" -ltnH)

	for port in "${PORTS[@]}"; do
		grep -q ":$port " <<< "$SS_OUT" || { echo "Listening port check failed for port $port" >&2; exit 1; }
	done
fi

# check supervisor status
if [[ -x $(which "$CMD_SUPERVISORCTL") ]]; then
	NUM_NOT_RUNNING=$("$CMD_SUPERVISORCTL" status | grep -cv "RUNNING\|EXITED\|STOPPED" || true)

	[[ ${NUM_NOT_RUNNING} == 0 ]] || { echo "Supervisor check failed" >&2; exit 1; }
fi

# explicit exit code
exit 0
