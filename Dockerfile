ARG TAG=3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c
FROM alpine:${TAG} as build

ARG TAG

# hadolint ignore=DL3019
RUN apk update \
    && apk add --virtual .build \
      alpine-sdk~=1 \
      git~=2 \
      sudo~=1 \
    && adduser -D build \
    && addgroup build abuild \
    && echo 'build ALL=(ALL) NOPASSWD: /bin/mkdir, /bin/cp' > /etc/sudoers.d/build

USER build

RUN USER=build abuild-keygen -a -n -i \
    && mkdir /home/build/tmp

USER root

RUN rm /etc/sudoers.d/build \
    && mkdir /app \
    && chown -R build:abuild /app \
    && chmod -R u+rwX,g+rwX,o+rX,o-w /app

WORKDIR /home/build/tmp

USER build

RUN git config --global init.defaultBranch master \
    && git init \
    && git config core.sparsecheckout true \
    && git sparse-checkout set "community/opendkim/*" \
    && git remote add origin -f git://git.alpinelinux.org/aports \
    && git pull --depth=1 origin "${TAG%%@*}-stable"

USER root

RUN mv community/opendkim /app/opendkim

# hadolint ignore=DL3019
RUN echo '/home/build/packages/app' >> /etc/apk/repositories \
    && apk update

WORKDIR /app/opendkim

RUN sed -i -e 's/\(sysconfdir.*\)/\1 \\/' -e '/sysconfdir/a \\t\t--with-odbx' APKBUILD \
    && sed -i 's/\(makedepends="\)/\1opendbx-dev /' APKBUILD \
    && apkgrel -a .

USER build

RUN abuild checksum \
    && abuild -r

FROM alpine:${TAG}

COPY files/ /
COPY --from=build /home/build/packages/app /tmp/pkgs
COPY --from=build /etc/apk/keys/build* /etc/apk/keys/

EXPOSE 10029

ENV OPENDKIM_SQL_DSN="mysql://user:pass@host/db" \
    OPENDKIM_REPORT_ADDRESS=""

RUN echo '/tmp/pkgs' >> /etc/apk/repositories \
    && apk add --no-cache \
        opendkim~=2 \
        opendkim-utils~=2 \
        opendbx~=1 \
        opendbx-backend-mysql~=1 \
        supervisor~=4 \
        inotify-tools~=4 \
        iproute2~=6 \
        bash~=5 \
        run-parts~=4 \
        gettext~=0 \
    && rm -R /tmp/pkgs \
    && install -d -o opendkim -g opendkim /run/opendkim

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/probe.sh"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]
CMD ["/usr/local/sbin/supervisord.sh"]
