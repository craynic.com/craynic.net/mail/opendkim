CREATE TABLE `opendkim_keytable` (
    `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `dkim_selector` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `dkim_key` mediumtext COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO `opendkim_keytable` (`domain`, `dkim_selector`, `dkim_key`) VALUES
    ('example.com', 'selector', '-----BEGIN RSA PRIVATE KEY-----\nMIICXgIBAAKBgQDCq6XzGWfKDj7x+POlJ2Jrt3R8lMo2WHqdsJe06ic1+CtziVyF\nl9+kdSb4PpLauMwwiqG+AF/mlaD0xYJqWLKrTZFujw78FVZcxzkC5/6Td6q++SDr\n/jf5S0SXVqR0n7czpAYSpSwuAWKKaxiE9XmU4jYmfXxuSTRfM8JxWKMPiwIDAQAB\nAoGAaSYb6fY0CICvYTXuLKvcToB2LACWRMp/2IMVPoQnj9AvcCWm0wYYp5Ub6oSp\n//hHWoTXHnDHXB5AJVY1vkmQn2NiRgBhh+q6cWKwUPou2V7UQ8AouKHlwpKHagzd\n5EIWQ6XhAgVV4DwyoZz5435UYQRotVXC3EBHNVcPa4Y+HdECQQDnXUQ74ylZHH++\n/UwpajofB7dvTM1HKDz43ySwlldPNFfjJSBVQg3wJ3pei7IGXPI+znQ0G6QezXzM\nJ/ZqcwYDAkEA12YmJ5S9mD647gqrfs2ZeSsVy+jmCN9Z6Kq10r25dRYHQH6JTabX\n4sCsH0xzLs+T1+6SeGmepLXyFrAmWWb92QJBAJ7qcg7Mhj8eEyTFaQXdYBSmQTni\nv/FLUy4CX3onmcregiuT+bR2DYrSIvxD6cDzILVBK5ILFlzN+hVksZ4Gaa8CQQDF\n2EmLaY+L23C5FIvY+LfTF8P5e35u/9fxYXuIikNiTE1068Tp0inRWO+//9R8VqWR\nXMykVT8mQwiN1lYjH/phAkEAsHtRDkMuhEIA05gA7E6yUb9QWLIhJvOlFvIzH9lW\nfGoLjJIxXAaDPdNpmVR/mhQwBNj1zZyFDMQzxBXKLzn57w==\n-----END RSA PRIVATE KEY-----');

CREATE TABLE `opendkim_signingtable` (
    `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO `opendkim_signingtable` (`domain`) VALUES
    ('example.com');
